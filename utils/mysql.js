const mysql      = require('mysql')
const code=require('../utils/code')
console.log(mysql)
// 创建数据池
const pool  = mysql.createPool({
    host     : '127.0.0.1',   // 数据库地址
    user     : 'root',    // 数据库用户
    password : 'root',   // 数据库密码
    database : 'membermanager'  // 选中数据库
})


// params 进行添加或修改的数据
function poolFn(connecQuery, sql, params) {
    // getConnection 创建连接池
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if(err) {
                reject('建立连接池失败')
                throw err
            }
            connecQuery(connection, sql, params).then(data => {
                connection.release() // 到这步说明已经完成操作，释放连接
                resolve(data)
            })
        })
        // console.log(pool._allConnections.length) // 连接池里的连接数
    })
}
/*
* connection 连接句柄
* sql 查询语句

* */

// 基于promise方法实现

// 查询数据
function find(connection, sql) {
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, data) => {
            // console.log(data)
            if(err) {
              reject(err)
                throw err
            }
            resolve(data)
        })
    })
}

// 添加数据
function add(connection, sql, params) {
    return new Promise((resolve, reject) => {
        connection.query(sql, params, (err, result) => {
            if(err) {
                resolve({
                    msg:'添加失败',
                    code:code.err,
                    data:null
                })
                throw err
            }
            resolve({
                msg:'添加成功',
                code:code.success,
                data:null
            })
        })
    })
}

// 删除数据
function del(connection, sql) {
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if(err) {
                resolve({
                    msg:'删除失败',
                    code:code.err,
                    data:null
                })
                throw err
            }
            resolve({
                msg:'删除成功',
                code:code.success,
                data:null
            })
        })
    })
}

// 修改数据
function update(connection, sql, params) {
    return new Promise((resolve, reject) => {
        connection.query(sql, params, (err, result) => {
            if(err) {
               reject(err)
                throw err
            }
            resolve(result)
        })
    })
}

// 将方法封装统一导出

function queryFn(connecQuery, sql, params) {
    return new Promise((resolve) => {
        poolFn(connecQuery, sql, params).then(data => {
            // console.log(data)
            resolve(data)
        })
    })
}

module.exports = {
    findData(sql, params) {
        return queryFn(find, sql, params)
    },
    addData(sql, params) {
        return queryFn(add, sql, params)
    },
    delData(sql, params) {
        return queryFn(del, sql, params)
    },
    updateData(sql, params) {
        return queryFn(update, sql, params)
    }
}


