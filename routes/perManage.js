const router = require('koa-router')()
const msql=require('../utils/processData.js')
const myCode=require('../utils/code.js')
router.prefix('/api')

//获取数据库信息  
router.get('/userlist', async function (ctx, next) {
    await msql.query(ctx,`SELECT * FROM users`)
})
router.post('/register', async function (ctx, next) {
    let username = ctx.request.body.username
    let password = ctx.request.body.password
    if(username && password){
        let result = await msql.query(ctx,`SELECT * FROM users WHERE username='${username}'`)
        if(result.length){
            ctx.body = {
                code:'0',
                msg:'用户名已存在',
                data:null
            }
        }else{
            let uuid = Math.floor(Math.random() * 900000000 + 100000000);
            await msql.add(ctx,`INSERT INTO users SET?`,{params:{username:username,password:password,uuid:uuid}})
            ctx.body = {
                code:'1',
                msg:'注册成功',
                data:null
            }
        }
    }else{
        ctx.body = {
            code:'0',
            msg:'请正确填写账号或密码',
            data:null
        }
    }
})
router.post('/login',async (ctx, next) => {
    let username = ctx.request.body.username
    let password = ctx.request.body.password
    let result = await msql.query(ctx,`SELECT * FROM users WHERE username='${username}' AND password='${password}'`)
    if(result.length){
        ctx.body = {
            code:'1',
            msg:'登录成功',
            data:result
        }
    }else{
        ctx.body = {
            code:'0',
            msg:'用户名或密码错误',
            data:null
        }
    }
    
})
module.exports = router
